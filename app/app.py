#  Copyright (c) 2020. Michael Kemna.
from flask import Flask
from powderbooking.utils import get_env_or_raise

from app.apis import api

app = Flask(__name__)
api.init_app(app)


@app.after_request
def after_request(response):
    response.headers.add("Access-Control-Allow-Origin", get_env_or_raise("CORS_HOST"))
    return response
