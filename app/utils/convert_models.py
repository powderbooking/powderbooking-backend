#  Copyright (c) 2020. Michael Kemna.
from typing import List

from flask_restx import fields, Model
from sqlalchemy import Table, Column


def convert_sqlalchemy_to_restx_model(table: Table) -> Model:
    """
    Convert an sqlalchemy table to a restx model.
    Adds support for the field types defined in map_sqlalchemy_to_restx_type, as well as description and required.

    :param table: The sqlalchemy table.
    :return: a restx model derived from the table.
    """
    map_sqlalchemy_to_restx_type = {
        "FLOAT": fields.Float,
        "INTEGER": fields.Integer,
        "VARCHAR": fields.String,
        "DATETIME": fields.DateTime,
        "DATE": fields.Date,
    }

    def _convert_sqlalchemy_to_restx_column(column: Column) -> (str, fields):
        """
        Inner function used by the map to convert each column.

        :param column: the sqlalchemy column.
        :return: a tuple with the name and field, usable for restx.
        """
        type: str = str(column.type)

        return column.name, map_sqlalchemy_to_restx_type[type](
            description=column.comment, required=column.nullable
        )

    results = map(_convert_sqlalchemy_to_restx_column, table.columns)

    return Model(table.name, {row[0]: row[1] for row in results})


def filter_restx_columns(model: Model, mask: List[str]) -> Model:
    """
    Filter the columns to your desired list that are specified in the mask.
    I favored this option instead of the built-in mask possibilities, because the mask can be overridden by the user.

    :param model: the current restx model.
    :param mask: a list of all columns that should be retained.
    :return: a restx model with only the columns that are in the given mask.
    """
    return Model(model.name, {key: model[key] for key in model if key in mask})
