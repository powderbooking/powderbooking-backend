#  Copyright (c) 2020. Michael Kemna.
from flask_restx import Namespace, Resource
from powderbooking.models import model_resort
from sqlalchemy import MetaData

from app.database import db
from app.utils.convert_models import convert_sqlalchemy_to_restx_model

api = Namespace("resort", description="Details of the resort")

resort = convert_sqlalchemy_to_restx_model(table=model_resort(metadata=MetaData()))
api.add_model(name=resort.name, definition=resort)


@api.route("/<int(signed=True):resort_id>")
@api.param("resort_id", "The resort identifier")
@api.response(404, "Resort not found")
class Resort(Resource):
    @api.doc("get_resort")
    @api.marshal_with(resort)
    def get(self, resort_id: int):
        """Get overview details given its identifier"""
        result = db.execute(
            db.get_table("resort")
            .select()
            .where(db.get_table_column("resort", "id") == resort_id)
        )

        if result.rowcount == 1:
            return result.fetchone()
        api.abort(404)
