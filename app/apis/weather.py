#  Copyright (c) 2020. Michael Kemna.
from flask_restx import Namespace, Resource
from powderbooking.models import model_weather
from powderbooking.database import Query
from sqlalchemy import MetaData

from app.database import db
from app.utils.convert_models import convert_sqlalchemy_to_restx_model

api = Namespace("weather", description="Weather reports of a resort")

weather = convert_sqlalchemy_to_restx_model(table=model_weather(metadata=MetaData()))
api.add_model(name=weather.name, definition=weather)


@api.route("/<int(signed=True):resort_id>")
@api.param("resort_id", "The resort identifier")
@api.response(404, "No weather report for given overview identifier found")
class Weather(Resource):
    @api.doc("get_weather_report")
    @api.marshal_with(weather)
    def get(self, resort_id: int):
        """Get the latest weather report for the given overview identifier"""
        result = db.execute_query(Query.select_weather_resort_1d, resort_id=resort_id)

        if result.rowcount == 1:
            return result.fetchone()
        api.abort(404)
