#  Copyright (c) 2020. Michael Kemna.
from flask_restx import Namespace, Resource, Model, fields
from powderbooking.models import model_forecast_week
from sqlalchemy import MetaData

from app.apis.resort import resort
from app.database import db
from app.database.query import Query
from app.utils.convert_models import (
    filter_restx_columns,
    convert_sqlalchemy_to_restx_model,
)

api = Namespace(
    "overview", description="Overview of all resorts with aggregate forecast data"
)

filtered_resort = filter_restx_columns(
    model=resort, mask=["id", "lat", "lng", "village"]
)
forecast_week = convert_sqlalchemy_to_restx_model(
    table=model_forecast_week(metadata=MetaData())
)
filtered_forecast_week = filter_restx_columns(
    model=forecast_week, mask=["rain_week_mm", "snow_week_mm"]
)

overview = Model(
    "overview", {**filtered_resort, **filtered_forecast_week}
)  # pythonic way to union two dicts
api.add_model(name=overview.name, definition=overview)

max_overview = Model(
    "max_overview",
    {
        "max": fields.Float(
            description="The maximum amount of snow or rain forecast of today",
            required=True,
        )
    },
)
api.add_model(name=max_overview.name, definition=max_overview)


@api.route("/")
class OverviewList(Resource):
    @api.doc("list_overview")
    @api.marshal_list_with(overview)
    def get(self):
        """List all resorts with aggregate forecast data of today"""
        return db.execute_query(Query.select_overview).fetchall()


@api.route("/max/<string:weather_type>")
@api.param("weather_type", "snow or rain")
@api.response(404, 'Inserted type should either be "snow" or "rain".')
class MaxOverview(Resource):
    @api.doc("max_overview")
    @api.marshal_with(max_overview)
    def get(self, weather_type: str):
        """Get the highest amount of snow or rain that is forecast today"""
        if weather_type == "snow":
            return db.execute_query(Query.select_max_overview_snow).fetchone()
        elif weather_type == "rain":
            return db.execute_query(Query.select_max_overview_rain).fetchone()
        else:
            api.abort(404)
