#  Copyright (c) 2020. Michael Kemna.
from flask_restx import Namespace, Resource
from powderbooking.models import model_forecast
from powderbooking.database import Query
from sqlalchemy import MetaData

from app.database import db
from app.database.query import Query as QueryLocal
from app.utils.convert_models import convert_sqlalchemy_to_restx_model

api = Namespace("forecast", description="Weather reports of a resort")

forecast = convert_sqlalchemy_to_restx_model(table=model_forecast(metadata=MetaData()))
api.add_model(name=forecast.name, definition=forecast)


@api.route("/current/<int(signed=True):resort_id>")
@api.param("resort_id", "The resort identifier")
@api.response(404, "No current forecast report for given overview identifier found")
class ForecastCurrent(Resource):
    @api.doc("get_current_forecast_report")
    @api.marshal_list_with(fields=forecast)
    def get(self, resort_id: int):
        """Get the current forecast report from today for the given overview
        identifier"""
        result = db.execute_query(Query.select_forecast_resort_1d, resort_id=resort_id)

        if result.rowcount > 0:
            return result.fetchall()
        api.abort(404)


@api.route("/past/<int(signed=True):resort_id>")
@api.param("resort_id", "The resort identifier")
@api.response(404, "No past forecast report for given overview identifier found")
class ForecastPast(Resource):
    @api.doc("get_past_forecast_report")
    @api.marshal_list_with(fields=forecast)
    def get(self, resort_id: int):
        """Get the past forecast reports of today for the given overview identifier"""
        result = db.execute_query(QueryLocal.select_forecast_past, resort_id=resort_id)

        if result.rowcount > 0:
            return result.fetchall()
        api.abort(404)
