#  Copyright (c) 2020. Michael Kemna.
from enum import Enum

from sqlalchemy import text


class Query(Enum):
    """
    This Enum is used to store all the queries that are used by the application.

    source to execute raw sql with sqlalchemy:
    https://chartio.com/resources/tutorials/how-to-execute-raw-sql-in-sqlalchemy/
    """

    select_forecast_past = text(
        """
        SELECT *
        FROM forecast
        WHERE resort_id = :resort_id
            AND date = current_date
        ORDER BY timepoint;
    """
    )

    select_overview = text(
        """
        SELECT r.id, r.village, r.lat, r.lng, f.rain_week_mm, f.snow_week_mm
        FROM resort as r
        JOIN forecast_week as f on r.id = f.resort_id
        WHERE current_date = f.date_request::date;
    """
    )

    select_max_overview_snow = text(
        """
        SELECT max(f.snow_week_mm)
        FROM resort as r
        JOIN forecast_week as f on r.id = f.resort_id
        WHERE current_date = f.date_request::date;
    """
    )

    select_max_overview_rain = text(
        """
        SELECT max(f.rain_week_mm)
        FROM resort as r
        JOIN forecast_week as f on r.id = f.resort_id
        WHERE current_date = f.date_request::date;
    """
    )
