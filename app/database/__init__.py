#  Copyright (c) 2020. Michael Kemna.
from powderbooking.database import build_database_handler, build_database_url

db = build_database_handler(database_url=build_database_url())
