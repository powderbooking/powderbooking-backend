#  Copyright (c) 2020. Michael Kemna.
from flask_restx import fields, Model
from sqlalchemy import Table, Column, Integer, String, Float, MetaData

from app.utils.convert_models import (
    convert_sqlalchemy_to_restx_model,
    filter_restx_columns,
)


def test_convert_sqlalchemy_to_restx_model():
    table: Table = Table(
        "overview",
        MetaData(),
        Column(
            "id", Integer, primary_key=True, comment="The identifier of the overview"
        ),
        Column(
            "continent", String, comment="The continent where the overview is located"
        ),
        Column(
            "lat",
            Float,
            comment="The latitudinal coordinate of the geolocation of the overview",
        ),
        Column(
            "altitude_min_m",
            Integer,
            comment="The lowest altitude of the overview (in metres)",
        ),
        Column("altitude_max_m", Integer),
    )

    expected: Model = Model(
        "overview",
        {
            "id": fields.Integer(description="The identifier of the overview"),
            "continent": fields.String(
                description="The continent where the overview is located"
            ),
            "lat": fields.Float(
                description="The latitudinal coordinate of the geolocation of the overview"
            ),
            "altitude_min_m": fields.Integer(
                description="The lowest altitude of the overview (in metres)"
            ),
            "altitude_max_m": fields.Integer(description=None),
        },
    )

    result: Model = convert_sqlalchemy_to_restx_model(table)

    assert_model_equivalence(result, expected)


def test_filter_restx_columns():
    model: Model = Model(
        "overview",
        {
            "id": fields.Integer(description="The identifier of the overview"),
            "continent": fields.String(
                description="The continent where the overview is located"
            ),
            "lat": fields.Float(
                description="The latitudinal coordinate of the geolocation of the overview"
            ),
            "altitude_min_m": fields.Integer(
                description="The lowest altitude of the overview (in metres)"
            ),
            "altitude_max_m": fields.Integer(description=None),
        },
    )

    mask = ["continent", "lat"]

    expected: Model = Model(
        "overview",
        {
            "continent": fields.String(
                description="The continent where the overview is located"
            ),
            "lat": fields.Float(
                description="The latitudinal coordinate of the geolocation of the overview"
            ),
        },
    )

    result: Model = filter_restx_columns(model, mask)

    assert_model_equivalence(result, expected)


def assert_model_equivalence(result: Model, expected: Model):
    assert isinstance(result, type(expected))
    assert result.name == expected.name
    assert len(result.items()) == len(expected.items())
    for key in expected.keys():
        assert key in result.keys()
        assert isinstance(result[key], type(expected[key]))
        assert expected[key].description == result[key].description
