#!/bin/sh

#
# Copyright (c) 2020. Michael Kemna.
#

set -e

# https://github.com/ufoscout/docker-compose-wait
/wait

echo "Starting app from entrypoint.."

exec "$@"
