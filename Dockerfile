FROM python:3.7.3-alpine3.9 as python-base

# set path for poetry: https://stackoverflow.com/a/61751745
# example using poetry https://github.com/wemake-services/wemake-django-template/blob/master/%7B%7Bcookiecutter.project_name%7D%7D/docker/django/Dockerfile
ENV \
  # python:
  # dump the traceback on error
  PYTHONFAULTHANDLER=1 \
  # write messages directly to stdout instead of buffering
  PYTHONUNBUFFERED=1 \
  # prevents python creating .pyc files
  PYTHONDONTWRITEBYTECODE=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_VERSION=1.1.3 \
  POETRY_HOME="/usr/local/poetry" \
  POETRY_VIRTUALENVS_IN_PROJECT=1 \
  # do not ask any interactive question
  POETRY_NO_INTERACTION=1 \
  POETRY_CACHE_DIR='/var/cache/pypoetry' \
  # wait:
  WAIT_VERSION=2.7.3 \
  # paths where everything gets installed
  PYSETUP_PATH="/usr/local/pysetup" \
  VENV_PATH="/usr/local/pysetup/.venv" \
  SRC_PATH="/src"

# prepend src, poetry and venv to path
ENV PATH="$SRC_PATH/:$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

# install runtime dependencies
# https://github.com/ufoscout/docker-compose-wait
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/${WAIT_VERSION}/wait /wait

RUN apk add --no-cache libpq && \
    chmod +x /wait

# ---------------------------------------------------
# ---------------------------------------------------
FROM python-base as builder-base

# install build dependencies
# psycopg2 source: https://hub.docker.com/r/svlentink/psycopg2/dockerfile
# uWSGI source: https://hub.docker.com/r/infrastructureascode/uwsgi/dockerfile
# PCRE source: https://stackoverflow.com/questions/21669354/rebuild-uwsgi-with-pcre-support
# alpine packages: https://pkgs.alpinelinux.org/packages
RUN apk add --no-cache linux-headers build-base postgresql-dev curl pcre2 pcre2-dev && \
    # install poetry:
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python

# copy project requirement files here to ensure they will be cached.
WORKDIR $PYSETUP_PATH
COPY poetry.lock pyproject.toml ./

RUN poetry install --no-root --no-dev --no-ansi

# ---------------------------------------------------
# ---------------------------------------------------
FROM python-base as development
WORKDIR $PYSETUP_PATH

# copy in our built poetry + venv
COPY --from=builder-base $POETRY_HOME $POETRY_HOME
COPY --from=builder-base $PYSETUP_PATH $PYSETUP_PATH

# quicker install as runtime deps are already installed
RUN poetry install

# will become mountpoint of our code
WORKDIR $SRC_PATH

EXPOSE 5000
ENTRYPOINT ["entrypoint.sh"]

# ---------------------------------------------------
# ---------------------------------------------------
FROM python-base as production

COPY --from=builder-base $PYSETUP_PATH $PYSETUP_PATH
COPY . $SRC_PATH

WORKDIR $SRC_PATH

RUN chmod +x ./entrypoint.sh && \
    # Create a non-root user
    adduser -D web && \
    chown web $SRC_PATH

USER web

EXPOSE 8080
ENTRYPOINT ["entrypoint.sh"]
CMD uwsgi --ini uwsgi.ini
